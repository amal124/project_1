

#Create a Security Group
    resource "aws_security_group" "sg1" {
    name        = "terraform_example"
    description = "Security Group "
    vpc_id      = "${var.VPC_id}"

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["10.0.0.0/16"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "sampleapp_VPCSG"
        Environment="DEV"
    }
    }



resource "aws_security_group" "RDS_sg" {

    name        = "terraform"
    description = "RDS Security Group "
    vpc_id      = "${var.VPC_id}"

  ingress {
  from_port         = 3306
  protocol          = "tcp"
  to_port           = 3306
 
  cidr_blocks       = ["0.0.0.0/0"]
}
}