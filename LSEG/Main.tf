

module "Foundation" {
  source = "./Foundation"
 Environment ="${var.Environment}"

  vpccidrblock = "${var.vpccidrblock}"
  publicsubnetCIDR = "${var.publicsubnetCIDR}"
  privatesubnet1CIDR = "${var.privatesubnet1CIDR}"
  privatesubnet2CIDR="${var.privatesubnet2CIDR}"
  appname = "${var.appname}"
  aws_region  = "${var.aws_region}"
  
}



module "SecurityGroup" {
  source = "./SecurityGroup"

  VPC_id = "${module.Foundation.VPC_id}"
  appname = "${var.appname}"
  aws_region  = "${var.aws_region}"
}
module "Instances" {
  source = "./Instances"

   appname = "${var.appname}"
   aws_region  = "${var.aws_region}"
  
   
   securitygroup1 = "${module.SecurityGroup.securitygroup1}"
   publicsubnetid = "${module.Foundation.publicsubnetid}"
   privatesubnet1id ="${module.Foundation.privatesubnet1id}"
   privatesubnet2id= "${module.Foundation.privatesubnet2id}"
}

module "RDS" {
  source = "./RDS"

  appname = "${var.appname}"
  aws_region  = "${var.aws_region}"
  RDS_sg = "${module.SecurityGroup.RDS_sg}"
  publicsubnetid = "${module.Foundation.publicsubnetid}"
  privatesubnet1id ="${module.Foundation.privatesubnet1id}"
  privatesubnet2id= "${module.Foundation.privatesubnet2id}"
}
