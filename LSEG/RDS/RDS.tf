resource "aws_db_subnet_group" "rds-private-subnet-group" {
  name = "rds-private-subnet-group"
  subnet_ids = ["${var.privatesubnet1id}","${var.privatesubnet2id}"]
}

resource "aws_db_instance" "rds" {
   
    identifier = "database-lseg-tcs"

  engine            = "mysql"
  engine_version    =  "5.7.19"
  instance_class    = "db.t2.large"
  allocated_storage = 20
  storage_type      = "gp2"
  storage_encrypted = "true"
 # kms_key_id        = var.kms_key_id
  #license_model     = var.license_model

  name                                = "database-lseg-tcs"
  username                            = " username"
  password                            = "Password"
  port                                = 3306
   

 #replicate_source_db = var.replicate_source_db

#snapshot_identifier = var.snapshot_identifier

vpc_security_group_ids = ["${var.RDS_sg}"]
db_subnet_group_name   = "${aws_db_subnet_group.rds-private-subnet-group.id}"
parameter_group_name   = "default-sqlserver-se-14.0"
  #option_group_name      = var.option_group_name
availability_zone   = "eu-west-1a"
backup_retention_period     = 7
backup_window               = "22:00-03:00"
multi_az = "true"
  #iops                = var.iops
publicly_accessible = "false"
max_allocated_storage = 100
deletion_protection = "true"

tags = {
    name = "RDS"
    Environment  = " Dev"
}
}
