

# Create a VPC to launch our instances into
    resource "aws_vpc" "VPC" {
    enable_dns_hostnames = true
    enable_dns_support = true
    cidr_block ="${var.vpccidrblock}"
  
    tags = {
       
        Environment = "Dev"
        ResourceName = "VPC"

    
    }
    }
    #Create a Application Subnet /public subnet
    resource "aws_subnet" "publicsubnet" {
    vpc_id                  = "${aws_vpc.VPC.id}"
    availability_zone       ="eu-west-1a"
    cidr_block              = "${var.publicsubnetCIDR}"
    map_public_ip_on_launch = true
 tags = {
     # Name =   "${join("TCS_${var.appname}-${var.Environment}-aws-public-subnet")}"
        Environment = "Dev"
        ResourceName = "VPC"

    
    }
    
    }

    # create a Database Subnet/private Subnet

    resource "aws_subnet" "privatesubnet1" {
    vpc_id                  = "${aws_vpc.VPC.id}"
    availability_zone       ="eu-west-1b"
    cidr_block              = "${var.privatesubnet1CIDR}"
    map_public_ip_on_launch = true
     tags = {
    #  Name =   "${join("TCS_${var.appname}-${var.Environment}-aws-private1-subnet")}"
        Environment = "Dev"
        ResourceName = "VPC"

    
    }
    }
    resource "aws_subnet" "privatesubnet2" {
    vpc_id                  = "${aws_vpc.VPC.id}"
    availability_zone       ="eu-west-1c"
    cidr_block              = "${var.privatesubnet2CIDR}"
    map_public_ip_on_launch = true
     tags = {
     #
     
     
      Name =   "${join("TCS_${var.appname}-${var.Environment}-aws-private2-subnet")}"
        Environment = "Dev"
        ResourceName = "VPC"

    
    }
    }

     resource "aws_eip" "eipnatgateway" {
    #instance = "${aws_instance.Web.id}"
    vpc      = true
    }

    #create a NAT Gateway

    resource "aws_nat_gateway" "natgateway" {
    allocation_id = "${aws_eip.eipnatgateway.id}" #Associate an EIP with Natgateway
    subnet_id     = "${aws_subnet.publicsubnet.id}"
    
    depends_on = ["aws_internet_gateway.igw"]
    }

    # Create an internet gateway 
    resource "aws_internet_gateway" "igw" {
    vpc_id = "${aws_vpc.VPC.id}"

    tags = {
        Name = "sampleapp_igw"
    }
    }

   
# Create Public route table
resource "aws_route_table" "publicroutetable" {
    vpc_id = "${aws_vpc.VPC.id}"
    
}

#Public Route 


resource "aws_route" "Public_route" {
    route_table_id = "${aws_route_table.publicroutetable.id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
    depends_on = [
        "aws_route_table.publicroutetable",
        "aws_internet_gateway.igw"
    ]
}
# Associate public route table to public subnet


resource "aws_route_table_association" "publicroutetable_association" {
    subnet_id = "${aws_subnet.publicsubnet.id}"
    route_table_id = "${aws_route_table.publicroutetable.id}"
}


#create Private Route table

    resource "aws_route_table" "privateroutetable" {
    vpc_id = "${aws_vpc.VPC.id}"

    
}

#Private  Route


resource "aws_route" "privateroute" {
    route_table_id = "${aws_route_table.privateroutetable.id}"
    destination_cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.natgateway.id}"
    depends_on = [
        "aws_route_table.publicroutetable",
        "aws_internet_gateway.igw"
    ]
}


# Associate private route table to Private subnet
resource "aws_route_table_association" "privateroutetable-association" {
    subnet_id = "${aws_subnet.privatesubnet1.id}"
    route_table_id = "${aws_route_table.privateroutetable.id}"
}

