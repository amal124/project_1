 
 
variable "vpccidrblock" {
  description =" Enter CIDR block for your VPC"

  default =  "10.0.0.0/16"
}

variable "publicsubnetCIDR" {
    description = "Enter CIDR block for public Subnet"
   
    default = "10.0.1.0/24"
}

variable "privatesubnet1CIDR" {
  description="Enter CIDR block for private Subnet"
    
  default ="10.0.2.0/24"
  
}
variable "privatesubnet2CIDR" {
  description="Enter CIDR block for private Subnet"
    
  default ="10.0.3.0/24"
  
}
variable "appname"{
description ="Enter Application Name"
}

variable "aws_region" {
  description = "Provide AWS Region"
  default ="eu-west-1"
}

variable "Environment" {
  description = "select the Environment "
 # type    = "string"
  # default = ["DEV", "PROD", "QA"]
    
    
}

