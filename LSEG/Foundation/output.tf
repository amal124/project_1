output "VPC_id" {
  value = "${aws_vpc.VPC.id}"
}

output "publicsubnetid" {
  value = "${aws_subnet.publicsubnet.id}"
}

output "privatesubnet1id" {
  value = "${aws_subnet.privatesubnet1.id}"
}
output "privatesubnet2id" {
  value = "${aws_subnet.privatesubnet2.id}"
}
