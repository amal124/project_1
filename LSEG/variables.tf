
variable "aws_access_key"{
description ="Enter Access Key"
type = "string"
}
variable "aws_secret_key"{
description ="Enter Secret Key"
type = "string"
}

variable "appname"{
description ="Enter Application Name"
type ="string"
}

variable "Environment" {
  description = "select the Environment "
  type    = "string"
  # default = ["DEV", "PROD", "QA"]
    
    
}


variable "aws_region" {
  description = "Provide AWS Region"
  default ="eu-west-1"
}

variable "vpccidrblock" {
  description =" Enter CIDR block for your VPC"

  default =  "10.0.0.0/16"
}

variable "publicsubnetCIDR" {
    description = "Enter CIDR block for public Subnet"
   
    default = "10.0.1.0/24"
}

variable "privatesubnet1CIDR" {
  description="Enter CIDR block for private Subnet"
    
  default ="10.0.2.0/24"
  
}
variable "privatesubnet2CIDR" {
  description="Enter CIDR block for private Subnet"
    
  default ="10.0.3.0/24"
  
}

variable "aws_amis" {
description ="Select AMI "
default = {
    eu-west-1 = "ami-00fc224d9834053d6"   
  }
  
}
